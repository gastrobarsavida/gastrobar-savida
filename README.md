Met SAVIDA zetten Olivier, Ellen en hun team een unieke gastrobar op de kaart in Aalst.
Verwacht u aan een verrassend gevarieerd aanbod van culinaire topgerechtjes, stuk voor stuk smaakbommetjes van formaat.
Bij een lekker bordje hoort een lekker glaasje, dus hebben we ook een pak topwijnen voor u.

Address: Kerkstraat 13, 9300 Aalst, Belgium

Phone: +32 477 49 59 28
